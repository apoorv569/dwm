# My dwm (dynamic window manager) build

![Screenshot of my desktop](https://gitlab.com/apoorv569/dotfiles/raw/master/.screenshots/desktop_dwm.png) 
Dwm is an extremely fast, small, and dynamic window manager for X. Dwm is created by the good folks at [suckless.org](https://suckless.org).  This is my personal build of dwm.  I used a number of patches in this build to make dwm more "sensible" rather than "suckless."  The patches I added to this build include:
+ actualfullscreen (actually toggles fullscreen for a window, instead of toggling the bar and the monocle layout)
+ attachaside (new clients appear in the stack rather than as the master)
+ autostart (a script that can be used to autostart certain programs)
+ bar-height (to adjust the height of the bar)
+ center (add an iscentered rule to automatically center clients on the current monitor)
+ centeredmaster (adding a centered master layout)
+ cfacts (for adding diffent weight to clients in the respective stack)
+ columns (adding a column layout)
+ cyclelayouts (cycles through the available layouts)
+ gridmode (adding a grid layout)
+ hide-vacant-tags (to hide unused tags)
+ noborder (remove the border when there is only one window visible)
+ monitorrules (Set default rules for pertag patch) -- By https://github.com/bakkeby
+ pertag (This patch keeps layout, mwfact, barpos and nmaster per tag)
+ restartsig (allows dwm to be restarted with a keybinding)
+ rotatestack (moves a window through the stack, in either direction)
+ savefloats (saves size and position of every floating window before it is forced into tiled mode)
+ namedscratchpads (Allows for the creation of multiple scratchpad windows)
+ statuscmd (adds the ability to click on the dwmblocks modules)
+ sticky (sticky client is visible on all tags)
+ systray (a simple system tray)
+ vanitygaps (adds gaps which can be adjusted on the run)

# Dependencies
+ ttf-joypixels
+ st
+ dmenu
+ clearine-git (AUR)
+ picom-tryone-git (AUR)
+ xfce4-power-manager
+ xfce4-settings
+ xfce-polkit-git (AUR)

Also, you will need to add the following from the AUR:
+ nerd-fonts-mononoki (or you can install nerd-fonts-complete for all the nerd fonts)
+ https://aur.archlinux.org/packages/libxft-bgra/ (needed for colored fonts and emojis)

Also, if you are building this on an Ubuntu-based system, you need to install libx11-dev and xorg-dev.

# Installing

Download the source code from this repository or use a git clone:

	git clone https://gitlab.com/apoorv569/dwm.git
	cd dwm
    sudo make clean install
	
NOTE: Installing this will overwrite your existing dwm installation.
	
# My Keybindings

The MODKEY is set to the Super key (aka the Windows key).  I try to keep the
keybindings consistent with all of my window managers.

| Keybinding | Action |
| :--- | :--- |
| `MODKEY + RETURN` | opens terminal (alacritty is the terminal but can be easily changed) |
| `ALT + SPACE` | opens run launcher (dmenu is the run launcher but can be easily changed) |
| `MODKEY + SHIFT + c` | closes window with focus |
| `MODKEY + SHIFT + CTRL + q` | restarts dwm |
| `MODKEY + SHIFT + q` | brings up clearine logout ui |
| `MODKEY + 1-9` | switch focus to workspace (1-9) |
| `MODKEY + SHIFT + 1-9` | send focused window to workspace (1-9) |
| `MODKEY + j` | focus stack +1 (switches focus between windows in the stack) |
| `MODKEY + k` | focus stack -1 (switches focus between windows in the stack) |
| `MODKEY + SHIFT + j` | rotate stack +1 (rotates the windows in the stack) |
| `MODKEY + SHIFT + k` | rotate stack -1 (rotates the windows in the stack) |

# Running dwm

If you do not use a login manager (such as lightdm) then you can add the following line to your .xinitrc to start dwm using startx:

    exec dwm
	
If you use a login manager (like lightdm), make sure that you have a file called dwm.desktop in your /usr/share/xsessions/ directory.  It should look something like this:

	[Desktop Entry]
	Encoding=UTF-8
	Name=Dwm
	Comment=Dynamic window manager
	Exec=dwm
	Icon=dwm
	Type=XSession

# Adding an autostart file

dwm has been patched in such a way that it looks for an autostart file at: $HOME/.dwm/autostart.sh

You will need to create this file and the directory that it is located.  An example autostart.sh is included below:

	#! /bin/bash 
	picom &
	nitrogen --restore &
	dwmblocks &
	
The example autostart.sh above launches the picom compositor, sets the wallpaper with nitrogen and launches dwmblocks to add some widgets to our dwm panel.  Obviously, you would need to install picom and nitrogen to use those programs in your autostart. And you would need to install [dwmblocks](https://gitlab.com/apoorv569/dwmblocks.git) to use it.  To use my dwmblocks, you also need to put the scripts in that are inside that statusbar folder in ~/.local/bin/statusbar.
